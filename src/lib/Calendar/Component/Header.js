import React, { Component } from 'react'
import { CalendarContext } from '../State/Globalstate';

export default class Header extends Component {
  static contextType = CalendarContext
  constructor(props){
    super(props)
    this.state = {
      monthName: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    }
  }
  
  render() {
    const { monthName } = this.state
    return <div className='header row flex-middle'>
      <div style={{ float: 'left', width: '100px' }}>
        <div className='icon' onClick={this.context.hideSidebar}>menu</div>
      </div>
      <div style={{ display: 'flex', float: 'right', width: 'calc(100% - 100px)' }}>
        <div className='col col-start' >
          <div className='icon' onClick={this.context.prevMonth}>
            chevron_left
      </div>
        </div>
        <div className='col col-center' >
          <span>
            {monthName[this.context.currentMonth]} {this.context.currentYear}
          </span>
        </div>
        <div className='col col-end' >
          <div className='icon' onClick={this.context.nextMonth}>
            chevron_right
      </div>
        </div>
      </div>
    </div>
  }

}