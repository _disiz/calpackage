import React, { Component } from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'

import { MyContext } from '../State/Globalstate'
import { deleteCalendar, addCalendar, editCalendar } from '../actions/apiCall';
import { showCalendar } from '../actions/calendar';
import { calSharing } from '../actions/calendarShare';

class Sidebar extends Component {
    static contextType = MyContext
    state={
        name: '',
        edit: '',
        color: '#c0c0c0',
        mod: 1
    }
    deleteCal = (id) => {
        this.props.deleteCalendar(id,this.context.currentMonth,this.context.currentYear)
    }
    checkCalendar = (e) => {
        let id = this.props.calendar_id
        if (e.target.checked){
            id.push(parseInt(e.target.id))
        } else {
            let index = id.indexOf(parseInt(e.target.id))
            id.splice(index, 1)
        }
        id =[...new Set(id)]
        this.props.showCalendar(id)
    }
    
    newCal = (id) => {
        const {edit} = this.state
        return <form onSubmit={()=> this.addCal(id)}>
        <input style={{background: 'transparent', width: '130px'}}
            placeholder={edit==='' ? 'New Calendar': null} value={this.state.name} 
            onChange={(e)=> this.setState({
                name: e.target.value
            })} disabled={!this.state.mod} required />
            <span style={{float: 'right'}}>
            <input type='color' style={{padding: '0px', width: '20px'}}  
                value={this.state.color} onChange={(e) => this.setState({color: e.target.value})} />
            <button className='icon' type='submit' style={{padding: '0px'}}>check</button>
            <button className='icon' type='button' style={{padding: '0px',marginRight: '10px'}} onClick={() => edit>0 ? this.setState({edit: '', name: '',mod: 1}) : this.cancelNewCal()}>close</button>
            </span>
            <br/>
        </form>
    }
    cancelNewCal = () => {
        this.setState({
            name: '',
            edit: '',
            mod: 1
        })
        this.context.addCalendar()
    }
    addingCal = () => {
        this.setState({
            edit: '',
            name: '',
            color: '#c0c0c0',
            mod: 1
        })
        this.context.addCalendar()
    }
    addCal = (id) => {
        const {currentMonth, currentYear} = this.context
        if(this.state.edit === '') {
            let data ={
                name: this.state.name,
                color: this.state.color
            }
            this.props.addCalendar(data,currentMonth,currentYear)
            this.context.addCalendar()
        } else {
            let data={
                name: this.state.name,
                color: this.state.color,
                status: 1
            }
            console.log(data)
            this.props.editCalendar(id,data,currentMonth,currentYear)
            this.setState({
                edit: '',
                name: '',
                color: '#c0c0c0',
                mod: 1
            })
        }
    }
    shareCal = (id) => {
        this.props.calSharing(id)
        this.props.history.push('/share')
    }

    render() {
        return (
            <div className='sidebar' >
                <br /><strong>My Calendars </strong>
                <button className='icon' onClick={() => this.addingCal()}>add_circle</button>
                <br />
                {this.context.forCalendar && this.state.edit=== '' ? this.newCal() : null} 
                {this.context.userCalendar.map((i,index) => {
                    return <div className='cal' style={{ fontWeight: '600'}} key={index}>
                    {this.state.edit !== index ? <><input id={i.id} key={i.id} type='checkbox'
                    checked={this.props.calendar_id.includes(i.id) ? 'checked' : ''}
                    style={{backgroundColor: i.color}}
                    onChange={(e) => this.checkCalendar(e)} />
                <label htmlFor={i.id}>{i.name}</label>
                <span style={{float: 'right',marginRight: '10px'}}>
                <button className='icon' onClick={() => this.shareCal(i.id)}>share</button>
                <button className='icon' 
                    onClick={()=> this.setState({edit: index, name: i.name, color: i.color, mod: i.is_modifiable})} >edit</button> 
                {i.is_modifiable === 1 ? <button className='icon' style={{}} onClick={() => this.deleteCal(i.id)}>delete</button> : null}
                </span>
                <br/> </>
                : this.newCal(i.id)}
                </div>
                })}
                <br/>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    calendar_id: state.calendar_id,
    user: state.user
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
    deleteCalendar: deleteCalendar,
    showCalendar: showCalendar,
    addCalendar: addCalendar,
    editCalendar: editCalendar,
    calSharing: calSharing
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Sidebar)