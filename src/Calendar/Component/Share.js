import React, { Component } from 'react'
import { connect } from 'react-redux'
import { shareToUserGroup } from '../actions/calendarShare';

class Share extends Component {
state = {
  user_group_id: '',
  guests: [],
  permissions: [],
  msg: null
}

  sharingCalendar = (e) => { 
    let data = {
      calendar_user_id: this.props.cal_share,
      user_group_id: this.state.user_group_id,
      guests: this.state.guests,
      permissions: this.state.permissions
    }
    if (this.state.user_group_id === ''  || this.state.guests.length === 0 || this.state.permissions.length === 0){
      this.setState({msg: 'Error! Please Select at least one guest and permission.'})
      e.preventDefault()
    } else {
      console.log(data)
      shareToUserGroup(data)
      this.props.history.push('/')
    }
}


  groupSelect = (e,i) => {
    if (e.target.checked) {
      this.setState({
          user_group_id: i
      })
  } else {
      this.setState({
          user_group_id: ''
      })  }
  }
  selectGroup = () => {
    return <>
      {this.props.user_group.map(i => {
        return <label key={i.id} style={{paddingLeft: '5px'}}><input type='radio' name='usergroup'
                onChange={(e) => this.groupSelect(e, i.id)}/>
                  {i.name}
              </label> 
      })}
    </>
  }

  checkGuest = (e,i) => {
    let guest = this.state.guests
      if (e.target.checked) {
        guest.push(i)
      } else {
        let a = guest.indexOf(i)
        if (a!== -1){
          guest.splice(a,1)
        }
      }
      this.setState({
        guests: guest
      })
  }
  guestList = () => {
    if (this.state.user_group_id !== '') {
        let guests = this.props.user_group.filter(i => {
        return this.state.user_group_id === i.id
      })
    return <ul style={{ marginLeft: '50px', listStyle: 'none' }}>
      {guests[0].users.map(i => {
        return <li key={i.email}><label>
          <input type='checkbox' onClick={(e) => this.checkGuest(e, i.email)} />{i.full_name}
          </label></li>
      })}
    </ul>
    }
  }

    checkPersmission = (e,i) => {
      let perm = this.state.permissions
      if (e.target.checked) {
        perm.push(i)
      } else {
        let a = perm.indexOf(i)
        if (a!== -1){
          perm.splice(a,1)
        }
      }
      this.setState({
        permissions: perm
      })
    }

    showPermission = () => {
      return <ul style={{marginLeft: '50px',listStyle: 'none'}}>
        {this.props.permission.map(i => {
          return <li key={i.id}><label><input type='checkbox' onClick={(e) => this.checkPersmission(e,i.name)}/>{i.display_name}</label></li>
        })}
        </ul>
    }
  
  render() {
    let calendarName = this.props.user_calendar.filter(i => {
      return i.id === this.props.cal_share
    })
    return (
      <div className='App-header'>
      { this.props.user_group && this.props.cal_share  ? 
        <form className='shareform' onSubmit={(e) => this.sharingCalendar(e)}>
        <button type='button' className='icon' style={{float: 'right'}} onClick={() => this.props.history.goBack()}>close</button><br/>
          Share <b>{calendarName[0].name}</b> with,<br/>
          <div style={{margin: '5px'}}>
          User Group : {this.selectGroup()}<br/>
          {this.state.user_group_id ? 'Guests:' : null}{this.guestList()}
          Allow Permissions : <br/>{this.showPermission()}
          {this.state.msg ?<p style={{lineHeight: '0.5', color: 'red'}}><small>{this.state.msg}</small></p>: null}
          </div>
          <button type='submit'>Share</button>
        </form> : 
      <div className = 'shareform'>
        <p style={{color: 'red'}}>There's something Wrong! </p>
        <button className='btn btn-light' onClick={() => this.props.history.push('/')}>Go Back To Calendar</button>
      </div>
    }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user,
  user_calendar: state.user_calendar,
  cal_share: state.cal_share,
  user_group: state.user_group,
  permission: state.permission
})


export default connect(
  mapStateToProps
)(Share)

