import React, {useContext} from 'react'
import { MyContext } from '../State/Globalstate';

export default function Header(){
    const fromContext = useContext(MyContext)
    const monthName = ['Jan', 'Feb', 'Mar', 'Apr', 'May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']

    return <div className='header row flex-middle'>
    <div style={{float:'left', width: '100px'}}>
    <div className='icon' onClick={fromContext.hideSidebar}>menu</div>
    </div>
    <div style={{display: 'flex',float:'right', width: 'calc(100% - 100px)'}}>
      <div className='col col-start' >
        <div className='icon' onClick={fromContext.prevMonth}>
          chevron_left
        </div>
      </div>
      <div className='col col-center' >
        <span>
          {monthName[fromContext.currentMonth]} {fromContext.currentYear}
        </span>
      </div>
      <div className='col col-end' >
        <div className='icon' onClick={fromContext.nextMonth}>
          chevron_right
        </div>
      </div>
    </div>
    </div>
  }