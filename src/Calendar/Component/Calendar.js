import React, { Component } from 'react'
import {connect} from 'react-redux'
import { MyContext } from '../State/Globalstate';

class Calendar extends Component { 
  static contextType = MyContext
  renderDays() {
    const dayName = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat']
    const day = dayName.map(i => <div key={i} className='col col-center'>
        {i}
    </div>)
    return <div className='days row'>{day}</div>
  }

  renderCells() {
    const { calendar } = this.context
    let months = Object.keys(calendar)
    if (parseInt(months[2])-parseInt(months[0])>10){
      let a = months[0]
      if(parseInt(months[2])-parseInt(months[1])===1){
        months[0] = months[1]
        months[1] = months[2]
        months[2] = a
      }else{
        months[0] = months[2]
        months[2] = months[1]
        months[1] = a
      }
    }
    const thisMonth =calendar[`${months[1]}`]
    const prevMonth = calendar[`${months[0]}`]
    const nextMonth = calendar[`${months[2]}`]
    if (thisMonth!== undefined){
    const rows = []
    let days = []
    let day = 0
    let noOfDays = thisMonth.length
    let prevMonthLength = prevMonth.length
    let exceedingEvent = [null,null,null]
    let previousEvent = []
    let lastDay = prevMonth[prevMonthLength -1].week_day+ 1
      for(let i =0; i< (lastDay < 7 ?prevMonthLength -lastDay : prevMonthLength); i++){
        let prevEvent = this.calendarEvent(prevMonth[i].events)
        for (let j=0; j<prevEvent.length; j++){
          let start = prevEvent[j].start_date.split(' ')[0]
          let end = prevEvent[j].end_date.split(' ')[0]
          let startMonth = parseInt(start.split('-')[1])
          let endMonth = parseInt(end.split('-')[1])
          let endDate = parseInt(end.split('-')[2])
          if ((endMonth > startMonth) || (endMonth === startMonth && endDate>prevMonthLength -lastDay)){
            previousEvent.push(prevEvent[j])
          }
        }
    }
    if (previousEvent.length){ 
      exceedingEvent = previousEvent
      if (exceedingEvent.length< 3){
        let appendArray =[]
        for (let i = 0; i<3-exceedingEvent.length;i++){
          appendArray.push(null)
        }
        exceedingEvent = [...exceedingEvent,...appendArray]
    }
  }
    let exceedinglength= [0,0,0]
    while (day < noOfDays) {
      let lastWeekDay = 7
      let eventLength = [0,0,0]
      let remainingEvents = [null,null,null]
      for (let i = 0; i < 7; i++) {  
        let dayArray = thisMonth[day]
        if(dayArray !== undefined) {
            if ((dayArray.week_day > i)){
            let diff = dayArray.week_day - i
            let prevDay = prevMonth[prevMonthLength - diff]
            let storedEvents = this.calendarEvent(prevDay.events)
            let presentEvent = storedEvents !== null ? i!== 0 ?
              this.displayEvent(storedEvents,prevDay,remainingEvents,eventLength)
              : this.displayEvent(storedEvents,prevDay,exceedingEvent,exceedinglength)
              : this.displayEvent([],prevDay,exceedingEvent,[0,0,0])
            days.push(<div 
                className='col cell' key={`${i}`}>
                <span className='bg' onClick={()=> this.context.prevMonth()} >
              <span className='number' style={{opacity: '0.3'}} >
                {prevDay.day}
              </span>
              <span className='text'>{prevDay.day}</span>  
              </span><br/>
              {storedEvents !== null ? 
                presentEvent.titles : ''}
              </div>)
              eventLength = presentEvent.eventLength
              remainingEvents = presentEvent.remainingEvents
          }else if (dayArray.week_day === i){
          let d = this.dateFormat(dayArray)
          let storedEvents = this.calendarEvent(dayArray.events)
          let presentEvent = i !==0 ? 
            this.displayEvent(storedEvents,dayArray,remainingEvents,eventLength)
            : this.displayEvent(storedEvents,dayArray,exceedingEvent,exceedinglength)
            days.push(
          <div key={i} className={`col cell ${dayArray.is_today ? 'current': ''}`}>
          <span className='bg' onClick={()=> this.context.dateSelect(d)} >
              <span className='number' >
                {dayArray.day}
              </span>
              <span className='text'>{dayArray.day}</span>  
              </span><br/>
              {storedEvents.length ? 
                presentEvent.titles : ''}
            </div> 
        )
        eventLength = presentEvent.eventLength
        remainingEvents = presentEvent.remainingEvents
        if (dayArray.week_day === 6){
          exceedingEvent = presentEvent.remainingEvents
          exceedinglength = presentEvent.eventLength
        }
        if (day < noOfDays) {
          day++
          lastWeekDay=dayArray.week_day
        }
      }      
    }if (lastWeekDay < i) {
      let diff = i - lastWeekDay
      let nextDay = nextMonth[diff-1]
      let storedEvents = this.calendarEvent(nextDay.events)
      let presentEvent = storedEvents !== null ? 
        this.displayEvent(storedEvents,nextDay,remainingEvents,eventLength):
        this.displayEvent([],nextDay,[null,null,null],[0,0,0])
      days.push(
                <div 
                  className='col cell' key={`${i}`}>
                  <span className='bg' onClick={()=> this.context.nextMonth()} >
                    <span className='number' style={{opacity: '0.3'}} >
                      {nextDay.day}
                    </span>
                    <span className='text'>{nextDay.day}</span>
                  </span><br />
                  {storedEvents !== null ?
                    presentEvent.titles : ''}
                </div>)
                eventLength = presentEvent.eventLength
                remainingEvents = presentEvent.remainingEvents
            }
    }
      rows.push(
        <div className='row' key={day}>
          {days}
        </div>
      )
      days = []
    }
    return <div className='body'>{rows}</div>}
  }

  calendarEvent = (e) => {
    let users = this.props.user_calendar
    let events = e.filter(e => this.props.calendar_id.includes(e.user_calendar_id))
    let allEvents = events.map(event => ({
          ...event,
          color: users.reduce((r,e) => {
            if (event.user_calendar_id === e.id){
               r=e
            }
             return r
          })
        })
      )
    return allEvents
  }
  
  displayEvent = (storedEvents,thisDay,remainingEvents,eventLength) => {
    let eventTitles = []
    let eventList = []
    let diff = 0
    let pending = eventLength
    let pendingEvents = remainingEvents
    let numberOfEvents = storedEvents.length
    let totalEvents=storedEvents
    for(let i=0;i<3;i++){
      if (remainingEvents[i] !== undefined && numberOfEvents !==0){
      if(remainingEvents[i] !== null){
        totalEvents.splice(i,0,remainingEvents[i])
    }
    } else if (remainingEvents[i] !== undefined && numberOfEvents ===0 ){
      totalEvents.push(remainingEvents[i]!== null ? remainingEvents[i] : {id:`blank${i}`, title: null})
    }
    }
    totalEvents = Array.from(new Set(totalEvents.map(a => a.id))).map(id => {return totalEvents.find(a=> a.id === id)})
    for (let i = 0; i < totalEvents.length; i++) {
      let event = <><p className='event' style={{ background: totalEvents[i].color ? totalEvents[i].color.color: '#c0c0c0'}} key={i}>
        {totalEvents[i].title}
      </p><br /></>
      if(totalEvents[i].title !== null){
        eventList.push({event: event, detail: totalEvents[i]})
      }else totalEvents.splice(i,1,null)
  }
  let m = thisDay.month<10 ? '0'+thisDay.month : thisDay.month
  let d = thisDay.day<10 ? '0'+thisDay.day :thisDay.day
  let today = thisDay.year+'-'+m+'-'+d
    for (let i = 0; i < (totalEvents.length > 3 ? 3 : totalEvents.length); i++) {
      if(totalEvents[i]!== null){
      let start = totalEvents[i].start_date.split(' ')[0]
      let end = totalEvents[i].end_date.split(' ')[0]
      let startDate = parseInt(start.split('-')[2])
      let endDate = parseInt(end.split('-')[2])
      let noOfDays = this.context.calendar[`${parseInt(start.split('-')[1])}`].length
      diff = m.toString() === end.split('-')[1] ?
        endDate - thisDay.day : 
        thisDay.week_day=== 0 && startDate !== thisDay.day ? 
        endDate > thisDay.day && m === end.split('-')[1]? 
        endDate - thisDay.day 
        : noOfDays - thisDay.day + endDate
        : parseInt(end.split('-')[1])> thisDay.month+1 ? noOfDays + this.context.calendar[thisDay.month+1].length+endDate  : noOfDays - thisDay.day + endDate
        let event = (today === start) || thisDay.week_day===0 ? 
        <><p className='event' key={i} style={{ background: totalEvents[i].color.color,
          width: `calc(100% * ${thisDay.week_day+diff < 7 ?  diff : 6-thisDay.week_day } + 90%)`, overflow: 'hidden' }}
        onClick={() => this.context.seeDetail(totalEvents[i])} >{totalEvents[i].title}</p><br /></> :
         <><br/></>
      pending.splice(i,1,eventLength[i] === 0 ? diff : eventLength[i]-1)
      pendingEvents.splice(i,1,eventLength[i]>0 ? totalEvents[i]: null)
      eventTitles.push(event)}
    }
    eventTitles = eventList.length>3 ? [...eventTitles,<><p className='event' style={{background: 'grey',width:'90%', overfow: 'hidden'}}
      onClick={()=> this.context.seeList(eventList)}>{eventList.length-3} more </p> </>] : eventTitles
      return {
      titles: eventTitles,
      eventLength: pending,
      remainingEvents: pendingEvents
    }
  }

  dateFormat = (dayArray) => {
    let m = this.context.currentMonth + 1
    let d = dayArray.day
    m = m<10 ? '0'+m : m
    d = d<10 ? '0'+d : d
    return dayArray.year+'-'+m+'-'+d
  }
  render() {
    return (
      <div className='calendar'>
        {this.renderDays()}
        {this.renderCells()}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  calendar: state.calendar,
  currentMonth: state.currentMonth,
  calendar_id : state.calendar_id,
  user_calendar: state.user_calendar
}) 

export default connect(
  mapStateToProps
  )(Calendar)