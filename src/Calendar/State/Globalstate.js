import React, { Component } from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import {apiCall, eventCreate, eventUpdate, getNewCal } from '../actions/apiCall'
import { editEvent, setCurrentMonth } from '../actions/calendar';

export const MyContext = React.createContext()

class Globalstate extends Component {
  state = {
    clickedEvent: [],
    eventList: [],
    isOpen: false,
    forSidebar: true,
    forEvent: true,
    seeEvent: false,
    forReminder: false,
    forMore: false,
    showList: false,
    showReminder: true,
    selectedDate: '',
    forCalendar: false
  }

  componentDidMount(){
    const {apiCall} = this.props
    apiCall()
  }

  shouldComponentRender() {
    if(this.props.pending === false) {return false}
    return true
}


  nextMonth = () => {
    let y=parseInt(this.props.currentYear)
    let m = this.props.currentMonth+1
    m = m<10 ? '0'+m : m
    let n = this.props.currentMonth+4
    let yy = n > 12 ? y+1 : y
    n = n > 12 ? n-12 : n
    n= n<10 ? '0'+n : n
    let date = {
      email: this.props.user,
      start_date: y+'-'+m+'-01',
      end_date: yy+'-'+n+'-01'
    }
    this.props.getNewCal(date,this.props.currentMonth!==11 ? m:0)
  }

  prevMonth = () => {
    let y=parseInt(this.props.currentYear)
    let m = this.props.currentMonth-1
    let yy = m < 1 ? y-1 : y
    m = m < 1  ? 12+m : m 
    m = m<10 ? '0'+m : m
    let n = this.props.currentMonth+2
    n = n>12 ? n-12 : n
    y = n === 1 ? y+1 : y
    n= n<10 ? '0'+n : n
    let date = {
      email: this.props.user,
      start_date: yy+'-'+m+'-01',
      end_date: y+'-'+n+'-01'
    }
    this.props.getNewCal(date,this.props.currentMonth !== 0 ? this.props.currentMonth-1 : 11)    
  }

  addEvent = () => {
    this.setState({
      forEvent: true,
      forReminder: false,
    })
  }
  addReminder = () => {
    this.setState({
      forEvent: false,
      forReminder: true,
    })
  }

  addMore = () => {
    this.setState({
      forMore: !this.state.forMore,
    })
  }

  handleClick = () => {
    this.setState({
      isOpen: !this.state.isOpen,
      forMore: false,
      clickedEvent: [],
      eventList: [],
      showList: false
    })
    this.props.editEvent(false)
  }

  dateSelect = (day) => {
    this.setState({
      selectedDate: day,
      seeEvent: false,
      isOpen: !this.state.isOpen,
      forEvent: true,
    })
  }

  hideSidebar = () => {
    this.setState({
      forSidebar: !this.state.forSidebar,
      forMore: false
    })
  }

  hideReminder = () => {
    this.setState({
      showReminder: !this.state.showReminder,
      forReminder: !this.state.forReminder
    })
  }

  updateCalendar = (e) => {
    this.props.editing ? 
      this.props.eventUpdate(e,this.props.currentMonth,this.props.currentYear)
      : this.props.eventCreate(e,this.props.currentMonth,this.props.currentYear)
    this.setState({
      isOpen: false,
      forMore: false,
      clickedEvent: []
    })
    this.props.editEvent(false)
  }

  seeDetail = (e) => {
    this.setState({
      seeEvent: true,
      isOpen: true,
      forEvent: false,
      clickedEvent: e,
      showList: false,
      eventList: []
    })
  }

  editEvent = () => {
    this.props.editEvent(true)
    this.setState({
      seeEvent: false,
      forMore: true,
      forEvent: true,
      selectedDate: '',
    })
  }

  seeList = (eventList) => {
    this.setState({
      isOpen: true,
      eventList: eventList,
      showList: true,
      seeEvent: false,
    })
  }

  addCalendar = () => {
    this.setState({
      forCalendar: !this.state.forCalendar
    })
  }
 
  render() {
    const { 
      isOpen, showReminder, eventList, forCalendar,
      seeEvent, clickedEvent, forEvent,
      forMore, forReminder, forSidebar, showList,
       selectedDate } = this.state
      return (
      <MyContext.Provider value={{
        isOpen: isOpen,
        forCalendar: forCalendar,
        seeEvent:seeEvent,
        userCalendar: this.props.user_calendar,
        calendar: this.props.calendar,
        clickedEvent: clickedEvent,
        forSidebar: forSidebar,
        forEvent: forEvent,
        forMore: forMore,
        forReminder: forReminder,
        eventList: eventList,
        showList: showList,
        showReminder: showReminder,
        currentMonth: this.props.currentMonth,
        currentYear: this.props.currentYear,
        selectedDate: selectedDate,
        addMore: this.addMore,
        addEvent: this.addEvent,
        editEvent: this.editEvent,
        addReminder: this.addReminder,
        handleClick: this.handleClick,
        dateSelect: this.dateSelect,
        prevMonth: this.prevMonth,
        nextMonth: this.nextMonth,
        hideSidebar: this.hideSidebar,
        hideReminder: this.hideReminder,
        updateCalendar: this.updateCalendar,
        seeDetail: this.seeDetail,
        seeList: this.seeList,
        addCalendar: this.addCalendar
      }}>
        {this.props.children}
      </MyContext.Provider>
    )
  }
}

const mapStateToProps = state => ({
  calendar: state.calendar,
  error: state.error,
  pending: state.pending,
  editing: state.editing,
  user: state.user,
  user_calendar: state.user_calendar,
  currentMonth: state.currentMonth,
  currentYear: state.currentYear
})

const mapDispatchToProps = dispatch => bindActionCreators({
  apiCall: apiCall,
  eventCreate: eventCreate,
  eventUpdate: eventUpdate,
  setCurrentMonth: setCurrentMonth,
  getNewCal: getNewCal,
  editEvent: val => dispatch(editEvent(val))
},dispatch)
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Globalstate)
