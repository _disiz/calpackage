export const FETCH_PENDING = 'FETCH_PENDING'
export const FETCH_CALENDAR = 'FETCH_CALENDAR'
export const FETCH_CALENDAR_ERROR = 'FETCH_CALENDAR_ERROR'
export const EDIT_EVENT = 'EDIT_EVENT'
export const LOGGED_USER = 'LOGGED_USER'
export const USER_LOGIN = 'USER_LOGIN'
export const CURRENT_MONTH = 'CURRENT_MONTH'
export const CURRENT_YEAR = 'CURRENT_YEAR'
export const SHOW_CALENDAR = 'SHOW_CALENDAR'
export const CHANGE_CALENDAR = 'CHANGE_CALENDAR'
export const SHARE_CALENDAR = 'SHARE_CALENDAR'
export const USER_GROUP = 'USER_GROUP'
export const SHARE_PERMISSION = 'SHARE_PERMISSION'

const fetchPending = () => ({
        type: FETCH_PENDING
})

const fetchCalendar = calendar => ({
        type: FETCH_CALENDAR,
        calendar: calendar
})

const fetchCalendarError = error => ({
        type: FETCH_CALENDAR_ERROR,
        error: error
})

const editEvent = editing => ({
        type: EDIT_EVENT,
        editing: editing
})

const userLogin = (id) => ({
        type: USER_LOGIN,
        user_calendar: id,
})

const loggedUser = (id) => ({
        type: LOGGED_USER,
        user: id
})

const setCurrentMonth = (id) => ({
        type: CURRENT_MONTH,
        currentMonth: id,
})
const setYear = (yr) => ({
        type: CURRENT_YEAR,
        currentYear: yr
})

const changeCalendar = (i,cal) => ({
        type: CHANGE_CALENDAR,
        currentMonth: i,
        calendar: cal
})

const showCalendar = (id) => ({
        type: SHOW_CALENDAR,
        calendar_id: id
})

const shareCalendar = (i) => ({
        type: SHARE_CALENDAR,
        cal_share: i,
})

const getUserGroup = (i) => ({
        type: USER_GROUP,
        user_group: i,
})

const sharePermission = (i) => ({
        type: SHARE_PERMISSION,
        permission: i,
})

export {
    fetchPending,
    fetchCalendar,
    fetchCalendarError,
    editEvent,
    userLogin,
    loggedUser,
    setCurrentMonth,
    setYear,
    showCalendar,
    changeCalendar,
    shareCalendar,
    getUserGroup,
    sharePermission
}