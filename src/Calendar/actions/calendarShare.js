import axios from 'axios'
import { shareCalendar, getUserGroup, sharePermission } from './calendar';

let baseUrl = 'https://backend-calendar.dev.genisys.cf/app'

export function calSharing(id) {
    let token = sessionStorage.getItem('calendar_access')
    let user = sessionStorage.getItem('calendar_user')    
    return dispatch=> {
        dispatch(shareCalendar(id))
        axios.get(baseUrl + '/groups', { params: {email: user},headers: { Authorization: 'Bearer ' + token } })
        .then(out => {
            console.log('group', out)
            dispatch(getUserGroup(out.data.data))
            axios.get(baseUrl + '/event/permissions', {params: {email: user}, headers: { Authorization: 'Bearer ' + token } })
                .then(out => {
                    console.log('permission', out)
                    dispatch(sharePermission(out.data.data))
                })
        })
        .catch(error => {
            console.error(error)
        })
    }
}

export function shareToUserGroup(d){
    let token = sessionStorage.getItem('calendar_access')
    let user = sessionStorage.getItem('calendar_user')    
    axios.post(baseUrl+'/share/calendars',d,{ params: {email: user},headers: { Authorization: 'Bearer ' + token } })
    .then(out => {
        console.log(out)
        return out
    })
    .catch(error => {
        console.log(error)
        return error
    })
}
