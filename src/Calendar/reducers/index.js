import {FETCH_PENDING, FETCH_CALENDAR, FETCH_CALENDAR_ERROR, EDIT_EVENT, USER_LOGIN, CURRENT_MONTH, SHOW_CALENDAR, LOGGED_USER, CHANGE_CALENDAR, CURRENT_YEAR, SHARE_CALENDAR, SHARE_PERMISSION, USER_GROUP} from '../actions/calendar'

const initialState = {
    pending: false,
    calendar: [],
    error: null,
    editing: false,
    user_calendar: [],
    user: sessionStorage.getItem('calendar_user'),
    calendar_id: [],
    currentMonth: 0,
    currentYear: 2019,
    cal_share: '',
    user_group: [],
    permission: []
}

export default function calendarReducer(state = initialState, action) {
    switch(action.type) {
        case FETCH_PENDING: 
            return {
                ...state,
                pending: true
            }
        case FETCH_CALENDAR:
            return {
                ...state,
                pending: false,
                calendar: action.calendar
            }
        case FETCH_CALENDAR_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        case EDIT_EVENT:
            return{
                ...state,
                editing: action.editing
            }

        case USER_LOGIN:
            return{
                ...state,
                user_calendar: action.user_calendar,
                calendar_id : action.user_calendar.map(i => i.id)
            }
        case LOGGED_USER:
            return{
                ...state,
                user: action.user
            }
        case CURRENT_MONTH: 
            return{
                ...state,
                currentMonth: action.currentMonth,
            }
        case CURRENT_YEAR: 
            return{
                ...state,
                currentYear: action.currentYear
            }
        case SHOW_CALENDAR:
            return{
                ...state,
                calendar_id: action.calendar_id
            }
        case CHANGE_CALENDAR:
            return{
                ...state,
                calendar: action.calendar,
                currentMonth: action.currentMonth,
            }
        case SHARE_CALENDAR:
            return{
                ...state,
                cal_share: action.cal_share,
            }
        case SHARE_PERMISSION:
            return{
                ...state,
                permission: action.permission
            }
        case USER_GROUP:
            return{
                ...state,
                user_group: action.user_group
            }
        
        
        default: 
            return state;
    }
}