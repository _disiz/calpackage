import React, { Component } from 'react'
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom'
import App from './App';
import Share from '../Component/Share'
import '../Component/Calendar.css'
import './App.css'
import 'bootstrap/dist/css/bootstrap.css'
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux'
import { apiCall } from '../actions/apiCall';
import { loggedUser } from '../actions/calendar';

class First extends Component {
  componentWillMount(){
    var user = sessionStorage.getItem('calendar_user')
    this.props.loggedUser(user)
    this.props.apiCall()
  }

  render() {
    return (
        <Router>
          <Switch>
            <Route path='/' component={App} />
            <Route path='/share' component={Share} />
          </Switch>
        </Router>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user
})

const mapDispatchToProps = dispatch => bindActionCreators({
  apiCall: apiCall,
  loggedUser: loggedUser
},dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(First)

