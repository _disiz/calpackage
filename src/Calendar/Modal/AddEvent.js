import React, { Component } from 'react'
import CKEditor from 'ckeditor4-react'
import { MyContext } from '../State/Globalstate';
import { connect } from 'react-redux';

class AddEvent extends Component {
    static contextType = MyContext
    state = {
        title: this.props.editing ? this.context.clickedEvent.title : '',
        errorDate: null,
        errorTitle: null,
        errorMsg: null,
        start_date: this.props.editing ? this.context.clickedEvent.start_date.split(' ')[0]: this.context.selectedDate || '2019-01-01',
        end_date: this.props.editing ? this.context.clickedEvent.end_date.split(' ')[0]: this.context.selectedDate || '2019-01-01',
        start_time: !this.props.editing ? '00:00' : this.context.clickedEvent.start_date.split(' ')[1].substr(0, 5),
        end_time: !this.props.editing ? '23:59' : this.context.clickedEvent.end_date.split(' ')[1].substr(0, 5),
        description: !this.props.editing ? ' ' : this.context.clickedEvent.description,
        visibility: !this.props.editing ? 'free' : this.context.clickedEvent.visibility,
        Notification: this.props.editing ? this.context.clickedEvent.reminder.length : 0,
        fullDay: this.context.clickedEvent.is_full_day_event === 0 ? false : true,
        repeat: this.context.clickedEvent.is_recurring === 1 ? 'Repeat' : 'Do Not Repeat',
        type: this.context.clickedEvent.is_recurring === 1 ? this.context.clickedEvent.formatted_rrule.recurring_type : 'daily',
        end: this.context.clickedEvent.is_recurring === 1 ?
            this.context.clickedEvent.formatted_rrule.recurring_by_until ? 'On' :
                this.context.clickedEvent.formatted_rrule.recurring_count ? 'After' : 'Never' : 'Never',
        interval: this.context.clickedEvent.is_recurring === 1 ? this.context.clickedEvent.formatted_rrule.recurring_interval : 1,
        byDay: this.context.clickedEvent.is_recurring === 1 && 
            this.context.clickedEvent.formatted_rrule.recurring_type === 'weekly' ?
              this.context.clickedEvent.formatted_rrule.recurring_by_day : [0],
        setPos: '1', 
        monthDay: this.props.editing ? parseInt(this.context.clickedEvent.start_date.split(' ')[0].split('-')[2]): this.context.clickedEvent.is_recurring === 1 && this.context.clickedEvent.formatted_rrule.recurring_by_month_day ?
            this.context.clickedEvent.formatted_rrule.recurring_by_month_day : parseInt(this.context.selectedDate.split('-')[2]),
        byMonth: this.props.editing ? parseInt(this.context.clickedEvent.start_date.split('-')[1]): this.context.clickedEvent.is_recurring === 1 ?
            this.context.clickedEvent.formatted_rrule.recurring_by_month : parseInt(this.context.selectedDate.split('-')[1]),
        location: this.props.editing ? this.context.clickedEvent.location: '',
        count: this.context.clickedEvent.is_recurring === 1 ? this.context.clickedEvent.formatted_rrule.recurring_count : 1,
        byUntil: this.props.editing ?
            this.context.clickedEvent.formatted_rrule.recurring_by_until ?
                this.context.clickedEvent.formatted_rrule.recurring_by_until.date.split(' ')[0] : this.context.selectedDate : this.context.selectedDate,
        on: true,
        privacy: this.props.editing ? this.context.clickedEvent.privacy : 'public',
        reminderArray: this.props.editing ? this.context.clickedEvent.reminder : [{
            notification_type: 'email',
            format: this.context.clickedEvent.is_full_day_event === 0 ? 'minutes': 'days',
            format_value: '10',
            time: '00:00',
        }],
        redit: '',
        calID: this.props.editing ? this.context.clickedEvent.user_calendar_id :this.context.userCalendar[0].id
    }

    startDate = () => {
        return <>
            <input style={{ width: '145px' }} type='date'
                defaultValue={this.state.start_date}
                onChange={(e) => this.setState({
                    start_date: e.target.value
                })} required/>
        </>
    }

    endDate = () => {
        return <>
            <input style={{ width: '145px' }} type='date'
                defaultValue={this.state.end_date} min={this.state.start_date}
                onChange={(e) => this.setState({
                    end_date: e.target.value
                })} required/>
        </>
    }

    startTime = () => {
        return <input type='time' step='900'
            defaultValue={this.state.start_time}
            onChange={(e) => this.setState({
                start_time: e.target.value
            })} required/>
    }

    endTime = () => {
        return <input type='time' step='900'
            defaultValue={this.state.end_time}
            onChange={(e) => this.setState({
                end_time: e.target.value
            })} required/>
    }

    addTime = () => {
        this.setState({
            fullDay: !this.state.fullDay,
        })
        if (this.props.editing && !this.state.fullDay) {
            this.setState({
                start_time: '00:00',
                end_time: '23:59'
            })
        }
    }

    repeatReminder = () => {
        const repitition = ['Do Not Repeat', 'Repeat']
        return <div>
            <div className='icon'
                style={{ padding: '5px' }} >replay</div>
            <select defaultValue={this.state.repeat}
                onChange={(e) => this.setState({
                    repeat: e.target.value
                })}>
                {repitition.map(i => {
                    return <option value={i} key={i}>{i}</option>
                })}
            </select>
            <input id='allDay' type='checkbox'
                onChange={() => this.addTime()}
                checked={!this.state.fullDay ? '' : 'checked'} />
            <label htmlFor='allDay'>All Day</label>
            {this.props.editing && this.context.clickedEvent.is_recurring === 1 && this.state.repeat === 'Repeat' ?
                <select style={{ marginLeft: '5px' }}
                    onChange={(e) => this.setState({
                        redit: e.target.value
                    })} >
                    <option>Edit</option>
                    <option value='single'>This Event</option>
                    <option value='future'>This and Following Events</option>
                    <option value='all'>All Events</option>
                </select> : ''}
            {this.state.repeat === 'Repeat' ?
                this.customRepeat() : ''}
        </div>
    }

    handleRecurringDays = (e, i) => {
        var recurringDays = this.state.byDay
        if (e.target.checked) {
            recurringDays.push(i)
            this.setState({
                byDay: recurringDays
            })
        } else {
            recurringDays = recurringDays.filter(day => {
                return day !== i
            })
            this.setState({
                byDay: recurringDays
            })
        }
    }
    handleEnd = (e) => {
        if (this.state.end === 'After') {
            this.setState({
                count: e.target.value,
                byUntil: ''
            })
        } else if (this.state.end === 'On') {
            this.setState({
                count: '',
                byUntil: e.target.value
            })
        }
    }

    customRepeat = () => {
        const type = ['daily', 'weekly', 'monthly', 'yearly']
        let month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        let days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
        //let position = ['First', 'Second', 'Third', 'Fourth']
        let end = ['Never', 'After', 'On']
        let endSelect = <></>
        let text = ''
        let button = <div></div>
        switch (this.state.end) {
            case 'Never':
                endSelect = <></>
                break;
            case 'After':
                endSelect = <><input style={{ width: '50px' }} value={this.state.count}
                    type='number' min='1' max='100' onChange={(e) => this.handleEnd(e)} /> occurences
                </>
                break;
            case 'On':
                endSelect = <><input type='date' style={{ width: '140px' }} min={this.state.start_date} value={this.state.byUntil} onChange={(e) => this.handleEnd(e)} required /></>
                break;

            default:
                break;
        }
        switch (this.state.type) {
            case 'daily':
                text = 'days'
                break;
            case 'weekly':
                text = 'weeks'
                button = <span className='btn-group' >{days.map((i, index) => {
                    return <label key={i}><input type='checkbox'
                        onChange={(e) => this.handleRecurringDays(e, index)}
                        defaultChecked={this.state.byDay.includes(index)} />
                        {i}</label>
                })}<br/>
                </span>
                break;
            case 'monthly':
                let months = Object.keys(this.context.calendar)
                text = 'months'
                button = <><br /><label htmlFor='on' style={{ marginLeft: '45px' }}>
                    <input type='radio' name='on' id='on'
                        defaultChecked={this.state.on}
                        onChange={() => this.setState({
                            on: !this.state.on
                        })} />on day
                    <input style={{ width: '50px' }} type='number'
                        value={this.state.monthDay} onChange={(e) => this.setState({
                            monthDay: e.target.value
                        })} max={this.context.calendar[`${months[1]}`].length} min='1' disabled={!this.state.on} /></label>
                    {/* <label htmlFor='on the' style={{marginLeft: '45px'}}><input type='radio'
                        name='on' id='on the'
                        onChange={() => this.setState({
                            on: !this.state.on
                        })} />on the
                    <select onChange={(e) => this.setState({
                            setPos: e.target.value
                        })} disabled={this.state.on}>
                            {position.map((i, index) => {
                                return <option value={index + 1} key={i}>{i}</option>
                            })}
                        </select>
                        <select disabled={this.state.on}
                            onChange={(e) => this.setState({
                                byDay: [e.target.value]
                            })}>
                            {days.map((i, index) => {
                                return <option value={index} key={i}>{i}</option>
                            })}
                        </select></label> */}
                </>
                break;
            case 'yearly':
                text = 'years'
                button = <><br /><label htmlFor='on' style={{ marginLeft: '45px' }}>
                    <input type='radio' id='on' name='on'
                        defaultChecked={this.state.on}
                        onChange={() => this.setState({
                            on: !this.state.on
                        })} />on day
                <select disabled={!this.state.on}
                        value={this.state.byMonth}
                        onChange={(e) => this.setState({
                            byMonth: e.target.value
                        })}>
                        {month.map((i, index) => {
                            return <option value={index + 1} key={i}>{i}</option>
                        })}
                    </select>
                    <input style={{ width: '50px' }} type='number'
                        value={this.state.monthDay} disabled={!this.state.on}
                        onChange={(e) => this.setState({
                            monthDay: e.target.value
                        })} max='32' min='1' />
                </label>
                    {/* <label htmlFor='on the' style={{marginLeft: '45px'}}>
                        <input type='radio' id='on the' name='on' onChange={() => this.setState({
                            on: !this.state.on
                        })} />
                        on the
                <select disabled={this.state.on}
                            onChange={(e) => this.setState({
                                setPos: e.target.value
                            })}>
                            {position.map((i, index) => {
                                return <option value={index + 1} key={i}>{i}</option>
                            })}
                        </select>
                        <select disabled={this.state.on}
                            onChange={(e) => this.setState({
                                byDay: [e.target.value]
                            })} >
                            {days.map((i, index) => {
                                return <option value={index} key={i} >{i}</option>
                            })}
                        </select>
                        of <select disabled={this.state.on}
                            onChange={(e) => this.setState({
                                byMonth: e.target.value
                            })}>
                            {month.map((i, index) => {
                                return <option value={index + 1} key={i}>{i}</option>
                            })}
                        </select></label> */}
                </>
                break;
            default:
                break;
        }
        return <><br />
            <span style={{ marginLeft: '40px' }}>Repeat
        <select defaultValue={this.state.type}
                    onChange={(e) => this.setState({
                        type: e.target.value
                    })}>
                    {type.map(i => {
                        return <option value={i} key={i}>{i}</option>
                    })} </select> <br /></span>
            <span style={{ marginLeft: '40px' }}>every <input style={{ width: '50px' }} type='number'
                max='100' min='1' value={this.state.interval}
                onChange={(e) => this.setState({ interval: e.target.value })}
                />
                {text}</span>
            <span style={{ marginLeft: '40px' }}>{button}</span>
            {this.state.errorMsg !== null ? <p style={{color: 'red', fontSize: 'small',marginLeft: '40px'}}>{this.state.errorMsg}</p>: null}
            <span style={{ marginLeft: '40px' }}>Ends <select defaultValue={this.state.end}
                onChange={(e) => this.setState({
                    end: e.target.value
                })}>
                {end.map(i => {
                    return <option value={i} key={i}>{i}</option>
                })}
            </select> {endSelect}</span><br />
        </>

    }

    event = () => {
        const { forReminder, forEvent, forMore } = this.context
        if (forEvent) {
            return <div style={{ margin: '5px' }}>
                <div className='icon'
                    style={{ padding: '5px' }} >access_time</div>
                {this.startDate()}
                {!this.state.fullDay ? this.startTime() : ''}-
                {!this.state.fullDay ? this.endTime() : ''}
                {this.endDate()}
                {this.state.fullDay ?
                    <button className='btn btn-light' type='button'
                        onClick={() => this.addTime()} >Add Time</button>
                    : ''}
                <span style={{ color: 'red' }}><br />{this.state.errorDate}</span>
                {forMore ? this.repeatReminder() : ''}
            </div>
        } else if (forReminder) {
            return <div style={{ margin: '5px' }}>
                <div className='icon'
                    style={{ padding: '5px' }} >access_time</div>
                {this.startDate()}
                {!this.state.fullDay ? this.startTime() : ''}<br />
                {this.repeatReminder()}
            </div>
        }
    }
    
    handleNotificationType = (e,i) => {
        let rem = this.state.reminderArray
        rem[i].notification_type = e
        this.setState({
            reminderArray: rem
        })
    }

    handleNotificationValue = (e,i) => {
        let rem = this.state.reminderArray
        rem[i].format_value = e
        this.setState({
            reminderArray: rem
        })
    }
    
    handleNotificationFormat = (e,i) => {
        let rem = this.state.reminderArray
        rem[i].format = e
        this.setState({
            reminderArray: rem
        })
    }
    
    handleNotificationTime = (e,i) => {
        let rem = this.state.reminderArray
        rem[i].time = e
        this.setState({
            reminderArray: rem
        })
    }

    handleNotification = (e,event,i) => {
        event.preventDefault()
        if (e=== '-'){
        let rem = this.state.reminderArray.filter((a, index) => {
                return index !== i})
        this.setState({
            Notification: this.state.Notification - 1,
            reminderArray: rem
        })}
        if (e === '+'){
            const newReminder = this.state.reminderArray
            newReminder.push({
                notification_type: 'email',
                format: this.context.clickedEvent.is_full_day_event === 0 ? 'minutes': 'days',
                format_value: '10',
                time: '00:00',
            })
            this.setState({
                Notification: this.state.Notification + 1,
                reminderArray: newReminder
            })
        }
    }

    EventDetail() {
        let addNotification = []
        for (let i = 0; i < this.state.Notification; i++) {
            addNotification.push(<><span key={i}>
                <select value={this.state.reminderArray[i].notification_type}
                    onChange={(e) => this.handleNotificationType(e.target.value, i)}>
                    <option value='email'>Email</option>
                    <option value='notification'>Notification</option>
                </select>
                <input style={{ width: '50px' }} type='number' value={this.state.reminderArray[i].format_value}
                    min='1' max='60'
                    onChange={(e) => this.handleNotificationValue(e.target.value, i)} />
                {!this.state.fullDay ?
                    <span>
                        <select value={this.state.reminderArray[i].format}
                            onChange={(e) => this.handleNotificationFormat(e.target.value, i)}>
                            <option value='minutes'>Minutes</option>
                            <option value='hours'>Hours</option>
                            <option value='days'>Days</option>
                        </select>
                    </span> :
                    <span>
                        <select style={{ width: '70px' }} value={this.state.reminderArray[i].format}
                            onChange={(e) => this.handleNotificationFormat(e.target.value, i)}>
                            <option value='days'>Days</option>
                            <option value='weeks'>Weeks</option>
                        </select>before at
            <input type='time' step='1800' style={{ margin: '0px' }}
                            value={this.state.reminderArray[i].time}
                            onChange={(e) => this.handleNotificationTime(e.target.value, i)} />
                    </span>}
                <button className='icon' style={{ margin: '0px', padding: '0px' }}
                    onClick={(e) => this.handleNotification('-', e, i)}>close</button>
            </span><br /></>)
        }
        const calendarSelect = () => {
            return <select value={this.state.calID}
                        onChange={(e) => this.setState({
                            calID: e.target.value
                        })}>
                        {this.context.userCalendar.map((i) => {
                            return <option style={{background: i.color}} value={i.id} key={i.id}>{i.name}</option>
                        })}
                    </select>
        }
        const Privacy = () => {
            return <select defaultValue={this.state.privacy}
            onChange={(e) => this.setState({
                privacy: e.target.value
            })}>
            <option value='public'>Public</option>
            <option value='private'>Private</option>
        </select>
        }
        const Visibility = () => {
            return <select defaultValue={this.state.visibility}
                onChange={(e) => this.setState({
                    visibility: e.target.value
                })}>
                <option value='free'>Free</option>
                <option value='busy'>Busy</option>
            </select>
        }
        return <div style={{
            borderTop: '1px solid  #eee',
            color: 'black',
            padding: '0px',
            margin: '0'
        }} >
            <div style={{
                flexDirection: 'column', width: '80%'
            }}>
                <h5>Event Details</h5>
                <div className='icon' style={{ padding: '5px' }} >
                    location_on
                </div>
                <input style={{ width: '60%' }}
                    placeholder='Location' value={this.state.location}
                    onChange={(e) => this.setState({
                        location: e.target.value
                    })} /><br />
                {addNotification}
                <button type='button' onClick={(e) => this.handleNotification('+', e)}>
                    Add Notification</button><br />
                <div className='icon' style={{ padding: '5px' }} >calendar_today</div>                
                {calendarSelect()}
                <div className='icon' style={{ marginLeft: '10px',padding: '5px' }} >card_travel</div>                
                {Visibility()}
                <div className='icon' style={{ marginLeft: '10px',padding: '5px' }} >visibility</div>                
                {Privacy()}
                <div style={{
                    padding: '0px',
                    border: '1px, solid #eee',
                    width: '450px'
                }}>
                    <CKEditor config={{ height: '60px',removePlugins : 'elementspath,resize',
                        removeButtons: 'Anchor,Styles,Format,Indent,Outdent,SpecialChar,Scayt,About,Source,Copy,Cut,Paste,Undo,Redo,Link,Unlink,Image,Print,Form,Button,SelectAll,NumberedList,BulletedList,CreateDiv,HorizontalRule,Table,PasteText,PasteFromWord,Maximize'
                    }} placeholder='Add Description'
                        data={this.state.description}
                        onChange={(e) => this.setState({
                            description: e.editor.getData()
                        })} />
                </div>
            </div>
        </div>
    }

    eventOrReminder = () => {
        const { forEvent, addEvent, addReminder, forMore } = this.context
        return !forMore ? <>
            <button className='btn btn-light'
                onClick={(e) => addEvent()}  type='button'
                style={{
                    margin: '10px', marginTop: '2px',
                    borderColor: forEvent ? '#d3d9df' : ''
                }}>Event</button>
            <button className='btn btn-light'
                onClick={(e) => addReminder()} type='button'
                style={{
                    margin: '10px',marginTop: '2px',
                    borderColor: !forEvent ? '#d3d9df' : ''
                }}>Reminder</button><br /></> :null
    }

    checkData = (e) => {
        const startingDate = new Date(this.state.start_date)
        const endingDate = new Date(this.state.end_date)
        let count = 0
        if (startingDate > endingDate) {
            this.setState({
                errorDate: 'Error in Event ending date!!'
            })
            count++
        }
        if (!this.state.title) {
            this.setState({
                errorTitle: 'Please Enter Title!!'
            })
            count++
        }
        if (this.state.type === 'weekly' && this.state.byDay.length=== 0){
            this.setState({
                errorMsg: 'Select at least One day of the week'
            })
            count++
        }
        if (count !== 0) {
            e.preventDefault()
        } else this.saveData()
    }

    saveData = () => {
        let endCondition = {}
        let recurringEvent = {}
        let recurringEdit = {}
        let origin_id = {}
        let rem = {}
        let reminder = []
        var newEvent = {
            user_calendar_id: this.state.calID,
            id: this.props.editing ? this.context.clickedEvent.id : '',
            title: this.state.title,
            description: this.state.description,
            is_full_day_event: this.state.fullDay ? 1 : 0,
            is_recurring: this.state.repeat === 'Repeat' ? 1 : 0,
            location: this.state.location,
            note: '',
            start_date: this.state.start_date + ' ' + this.state.start_time + ':00',
            end_date: this.state.end_date + ' ' + this.state.end_time + ':00',
            email: this.props.user,
            created_by: this.props.user,
            status: 1,
            guests: this.props.editing ? this.context.clickedEvent.guests: [],
            event_permission: this.props.editing ? this.context.clickedEvent.event_permission : [],
            visibility: this.state.visibility,
            recurring_type: this.state.repeat === 'Repeat' ? this.state.type : '',
            recurring_interval: this.state.repeat === 'Repeat' ? this.state.interval : '',
            has_reminder: this.state.Notification > 0 ? 1 : 0,
            privacy: this.state.privacy,
        }
        if (this.state.Notification === 0){
            reminder={}
        }else {
        for(let i=0;i<this.state.Notification;i++){
            reminder[i]= {
                format: this.state.reminderArray[i].format,
                format_value:  this.state.reminderArray[i].format_value,
                time: this.state.reminderArray[i].time.length === 5 ? this.state.reminderArray[i].time+':00':this.state.reminderArray[i].time,
                note: '',
                notification_type: this.state.reminderArray[i].notification_type,
                id: this.props.editing ? this.state.reminderArray[i].id : null
            }
        }}
        rem={reminder}
        if (this.state.repeat === 'Repeat' ) {
            switch (this.state.type) {
            case 'weekly':
                recurringEvent = {
                    recurring_by_day: this.state.repeat === 'Repeat' ? this.state.byDay : ''
                }
                break
            case 'monthly':
                if (this.state.on) {
                    recurringEvent = {
                        recurring_by_month_day: this.state.repeat === 'Repeat' ? this.state.monthDay : '',
                    }
                } else {
                    recurringEvent = {
                        recurring_by_day: this.state.repeat === 'Repeat' ? this.state.byDay : '',
                        //recurring_by_set_pos: this.state.repeat === 'Repeat' ? this.state.setPos : '',            
                    }
                }
                break
            case 'yearly':
                if (this.state.on) {
                    recurringEvent = {
                        recurring_by_month_day: this.state.repeat === 'Repeat' ? this.state.monthDay : '',
                        recurring_by_month: this.state.repeat === 'Repeat' ? this.state.byMonth : '',
                    }
                } else {
                    recurringEvent = {
                        recurring_by_day: this.state.repeat === 'Repeat' ? this.state.byDay : '',
                        //recurring_by_set_pos: this.state.repeat === 'Repeat' ? this.state.setPos : '',            
                        recurring_by_month: this.state.repeat === 'Repeat' ? this.state.byMonth : '',
                    }
                }
                break
            default: break
        }
        endCondition = this.state.end === 'After' ? {
            recurring_count: this.state.count
        } : this.state.end === 'On' ? {
            recurring_by_until: this.state.byUntil
        } : {}
        }
        if (this.context.clickedEvent.origid){
            origin_id = {
                origid: this.context.clickedEvent.origid,
            }
        }
        if (this.props.editing && this.context.clickedEvent.is_recurring === 1) {
                recurringEdit = {
                    redit: this.state.redit!== '' ? this.state.redit: 'single'
            }
        }
        newEvent = { ...newEvent, ...recurringEvent,...rem,...origin_id, ...endCondition, ...recurringEdit }
        this.context.updateCalendar(newEvent)
    }

    render() {
        const { addMore, forMore, forEvent } = this.context
        return (
            <form onSubmit={(e)=> this.checkData(e)}>
                <input placeholder='Add Title'
                    style={{ width: '80%', fontSize: '125%' }}
                    value={this.state.title}
                    onChange={(e) => this.setState({
                        title: e.target.value
                    })} required/>
                <p style={{ marginBottom: '5px',color: 'red' }}>{this.state.errorTitle}</p>
                {this.eventOrReminder()}
                {this.event()}
                {forEvent && !forMore ?
                    <button className='btn btn-default'
                        onClick={(e) => addMore()} type='button'>
                        More Options</button> : ''}
                {forMore ? this.EventDetail() : null}
                <button style={{ float: 'right' }} type='submit'
                    className='btn btn-primary'>
                    Save</button>
                </form>
        )
    }
}

const mapStateToProps = state => ({
    editing: state.editing,
    user: state.user
})

export default connect(
    mapStateToProps
)(AddEvent)
