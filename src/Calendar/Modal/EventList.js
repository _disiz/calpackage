import React, { Component } from 'react'
import { MyContext } from '../State/Globalstate';

export default class extends Component {
    static contextType = MyContext
    render() {
    return (
      <div> 
        { this.context.eventList.map((i,index)=> 
            < span key={index} onClick={() => this.context.seeDetail(i.detail)}>{i.event}</span>)}
      </div>
    )
  }
}
